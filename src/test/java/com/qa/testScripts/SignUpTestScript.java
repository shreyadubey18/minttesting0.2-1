package com.qa.testScripts;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SignUpTestScript {

	private WebDriver driver;
	private WebElement we;

	@Test
	public void createAccountForSingleUser() throws Exception {
        String driverPath = System.getProperty("user.dir") + "/src/test/java/Drivers/chromedriver.exe";
//		System.out.println(driverPath);
		System.setProperty("webdriver.chrome.driver","./src/main/resources/com/qa/driver/chromedriver.exe" );
		ChromeOptions options = new ChromeOptions();
		driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		driver.get("http://test-dev.ap-southeast-2.elasticbeanstalk.com/login");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		String title = driver.getTitle();
		System.out.println(title);
		Assert.assertEquals("Login V2", title);

		String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
		TakesScreenshot ts = (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		String destination = System.getProperty("user.dir") + "/FailedTestsScreenshots/" + dateName + ".png";
		File finalDestination = new File(destination);
		FileUtils.copyFile(source, finalDestination);

		Assert.assertTrue(true);
		WebElement we = driver.findElement(By.xpath("//a[@id='txt2']"));
		we.click();
		Thread.sleep(2000);
		we = driver.findElement(By.xpath("//fieldset[@id='step1']//input[@name='next']"));
		we.click();

		String userName = "" + (int) (Math.random() * Integer.MAX_VALUE);
		String emailID = "User" + userName + "@example.com";
		System.out.println(emailID);
		we = driver.findElement(By.xpath("//input[@id='email']"));
		we.sendKeys(emailID);
		Thread.sleep(100);
		we = driver.findElement(By.xpath("//input[@id='password']"));
		we.sendKeys("Shreya@1");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step11']//input[@name='next']"));
		we.click();

		we = driver.findElement(By.xpath("//div[contains(text(),'Individual Account')]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='fullName']"));
		we.sendKeys("Zeena");
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//input[@id='surname']"));
		we.sendKeys("ali");
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//input[@id='dob']"));
		we.click();
		Thread.sleep(1000);
		

		we = driver.findElement(By.xpath("//select[@class='ui-datepicker-year']/option[18]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//a[@class='ui-state-default']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(
				By.xpath("//div[@class='col-sm-6 form-group country-set flag-drop']//div[@class='selected-flag']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath(
				"//li[@class='country preferred active highlight']//span[@class='country-name'][contains(text(),'New Zealand')]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//div[@class='container']//option[45]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='input-occupation']"));
		we.sendKeys("Software Testing");
		Thread.sleep(1000);
		JavascriptExecutor jsx = (JavascriptExecutor) driver;
		jsx.executeScript("window.scrollBy(0,1000)");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step3']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
        we = driver.findElement(By.xpath("//input[@id='address']"));
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("document.getElementById('address').value='102 Hobson Street, Auckland CBD, Auckland, New Zealand'");
		
		we.sendKeys("102 Hobson Street, Auckland CBD, Auckland, New Zealand");
		Thread.sleep(5000);
		we = driver.findElement(
				By.xpath("//div[@class='mobile_number first-mobile top-show']//div[@class='selected-flag']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath(
				"//ul[@class='country-list']//li[@class='country preferred']//span[@class='country-name'][contains(text(),'India')]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='mobile']"));
		we.sendKeys("9729767778");
		Thread.sleep(2000);
		we = driver.findElement(By.xpath("//fieldset[@id='step4']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
		//we = driver.findElement(By.xpath("//fieldset[@id='step5']//li[1]"));
		//we.click();

		we = driver.findElement(By.xpath("//input[@id='verification']"));
		we.sendKeys("000000");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step5']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//select[@id='src_of_fund1']/option[2]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='passport_number']"));
		we.sendKeys("ANDHEH376");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='passport_issue_date']"));
		we.click();
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//select[@class='ui-datepicker-year']/option[81]"));
		we.click();
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//select[@class='ui-datepicker-month']/option[3]"));
		we.click();
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//a[contains(text(),'29')]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='dob2']"));
		we.click();
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//select[@class='ui-datepicker-year']/option[91]"));
		we.click();
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//select[@class='ui-datepicker-month']/option[3]"));
		we.click();
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//a[contains(text(),'29')]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//div[@class='row passport-select']//div[@class='selected-flag']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(
				By.xpath("//ul[@class='country-list']//span[@class='country-name'][contains(text(),'Australia')]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step6']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//select[@id='Investors_Rate']/option[2]"));
		we.click();
		Thread.sleep(2000);
		we = driver.findElement(By.xpath("//input[@id='IRD_Number']"));
		we.sendKeys("030658442");
		Thread.sleep(2000);
		we = driver.findElement(By.xpath("//select[@id='wealth_src']/option[4]"));
		we.click();
		Thread.sleep(2000);
		we = driver.findElement(By.xpath("//select[@class='selectoption selectoption1 form-group']/option[2]"));
		we.click();
		Thread.sleep(2000);
		we = driver.findElement(By.xpath("//a[@id='add-country-another']"));
		we.click();
		Thread.sleep(2000);
		we = driver.findElement(By.xpath("//select[@class='selectoption selectoption1 form-group']/option[1]"));
		we.click();
		Thread.sleep(2000);
		we = driver.findElement(By.xpath("//fieldset[@id='step7']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//div[@class='textfirst']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//li[contains(text(),'ASB Bank')]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='acount_holder_name']"));
		we.sendKeys("Zeena Ali");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='accountNumber']"));
		we.clear();
		we.sendKeys("1230860044496001");
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//input[@class=' checkname']"));
		we.sendKeys("C:\\Users\\Lenovo\\git2\\MintCode\\src\\main\\resources\\com\\qa\\driver\\IRF.jpg");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step8']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//span[@class='checkmark']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step9']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='submit']"));
		we.click();
		Thread.sleep(10000);
		
		System.out.println(emailID);
		driver.get("http://test-dev.ap-southeast-2.elasticbeanstalk.com/login?logout");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		we = driver.findElement(By.xpath("//button[@class='confirm']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='Email']"));
		we.clear();
		we.sendKeys(emailID);
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='password']"));
		we.clear();
		we.sendKeys("Shreya@1");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//button[@id='logbtn']"));
		we.click();

		String title1 = driver.getTitle();
		System.out.println(title);
		// Assert.assertEquals(title, "Beneficiary Dashboard");

		if (title == "Beneficiary Dashboard") {
			System.out.println("Individual Account" + emailID + "Successful");
		} else {
			System.out.println("Individual Account" + emailID + "UnSuccessful");
		}
		driver.quit();
	}

}
